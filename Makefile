# Package name, for license location.
pkgname=xsudo

# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

install: xsudo
	install -Dm755 ./xsudo $(DESTDIR)/$(PREFIX)/bin/xsudo
	install -Dm644 ./LICENSE $(DESTDIR)/$(PREFIX)/share/licenses/$(pkgname)/LICENSE

# Local Variables:
# inhibit-untabify: t
# End:
