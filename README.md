# xsudo

xsudo is a script taken from [this page](http://web.archive.org/web/20220428194115/https://wiki.lxde.org/en/PCManFM#pkexec_method) on the LXDE wiki as simple replacement for gksudo.
xsudo depends on the `pkexec` package.

## Installation

To install from git:

```
git clone https://gitlab.com/davisrichard437/xsudo/xsudo.git
cd xsudo
sudo make install
```

Alternatively, download and extract the [latest release](https://gitlab.com/davisrichard437/xsudo/-/releases/permalink/latest) and run `sudo make install` as above.
